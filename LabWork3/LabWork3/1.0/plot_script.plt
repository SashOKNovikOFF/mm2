reset

set xlabel "tau"
set ylabel "eta"

set palette rgbformulae 30,31,32

set terminal png size 700, 500
set output '1.png'

splot 'velocity.txt' matrix w pm3d ti ""

set terminal png size 700, 500
set output '2.png'

set view 90, 90

splot 'velocity.txt' matrix w pm3d ti ""