reset

set xlabel "y"
set ylabel "x"
set zlabel "u(x, y)"

set terminal png size 700, 500
set output 'K_50.png'

set yrange [0:5]
#set xrange [0:50]

set palette rgbformulae 30, 31, 32
set pm3d interpolate 2, 2

splot 'K_50.txt' matrix with pm3d notitle