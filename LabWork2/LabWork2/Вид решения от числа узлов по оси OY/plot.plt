reset

set xlabel "x"
set ylabel "y"
set zlabel "u(x, y)"

#set yrange [0:10]
#set xrange [0:50]

set palette rgbformulae 34, 35, 36
set pm3d interpolate 2, 2

splot 'M_10.txt' using 1:2:3 with lines, \
      'M_20.txt' using 1:2:3 with lines, \
      'M_40.txt' using 1:2:3 with lines, \
      'M_80.txt' using 1:2:3 with lines