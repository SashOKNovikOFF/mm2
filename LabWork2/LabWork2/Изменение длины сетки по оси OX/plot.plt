reset

set xlabel "x"
set ylabel "y"
set zlabel "u(x, y)"

#set yrange [0:10]
#set xrange [0:50]

set palette rgbformulae 34, 35, 36
set pm3d interpolate 2, 2

splot 'X_1.txt' matrix w lines, \
      'X_5.txt' matrix w lines, \
      'X_10.txt' matrix w lines, \
      'X_25.txt' matrix w lines, \
      'X_50.txt' matrix w lines