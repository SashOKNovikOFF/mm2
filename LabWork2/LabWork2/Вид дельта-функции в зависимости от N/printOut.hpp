#pragma once

#include <fstream>
#include <string>
#include <vector>
#include <iomanip>

const int WIDTH_COLUMN = 20;

void printOut(std::string fileName,                    // Название выходного файла
	          int accurancy,                           // Точность числовых данных
			  std::vector<std::string> header,         // Вектор заголовков
			  std::vector< std::vector<double> > data, // Вектор, содержащий вектора данных
			  bool showHeader,                         // Отображать заголовки или нет
			  bool showNumeration);                    // Отображать нумерацию или нет

void printOutWithoutHeader(std::string fileName,                    // Название выходного файла
	                       int accurancy,                           // Точность числовых данных
	                       std::vector< std::vector<double> > data, // Вектор, содержащий вектора данных
	                       bool showNumeration);                    // Отображать нумерацию или нет

void printOutOnlyData(std::string fileName,  // Название выходного файла
	              int accurancy,             // Точность числовых данных
	              std::vector<double> data); // Вектор данных

void printOut(std::string fileName, int accurancy, std::vector<std::string> header, std::vector< std::vector<double> > data, bool showHeader, bool showNumeration)
{
	// TODO: добавить проверку ввода данных

	std::ofstream file(fileName);

	file.precision(accurancy);
	file << std::scientific;

	if (showHeader)
	{
		if (showNumeration)
			file << std::setw(WIDTH_COLUMN) << "№";
		for (std::size_t i = 0; i < header.size(); i++)
			file << std::setw(WIDTH_COLUMN) << header[i];
		file << std::endl;
	};

	for (std::size_t i = 0; i < data[0].size(); i++)
	{
		if (showNumeration)
			file << std::setw(WIDTH_COLUMN) << i;
		for (std::size_t vec = 0; vec < data.size(); vec++)
			file << std::setw(WIDTH_COLUMN) << data[vec][i];
		file << std::endl;
	};

	file.close();
};

void printOutWithoutHeader(std::string fileName, int accurancy, std::vector< std::vector<double> > data, bool showNumeration)
{
	// TODO: добавить проверку ввода данных

	std::ofstream file(fileName);

	file.precision(accurancy);
	file << std::scientific;

	for (std::size_t i = 0; i < data[0].size(); i++)
	{
		if (showNumeration)
			file << std::setw(WIDTH_COLUMN) << i;
		for (std::size_t vec = 0; vec < data.size(); vec++)
			file << std::setw(WIDTH_COLUMN) << data[vec][i];
		file << std::endl;
	};

	file.close();
};

void printOutOnlyData(std::string fileName, int accurancy, std::vector<double> data)
{
	// TODO: добавить проверку ввода данных

	std::ofstream file(fileName);

	file.precision(accurancy);
	file << std::scientific;

	for (std::size_t i = 0; i < data.size(); i++)
	{
		file << data[i];
		file << std::endl;
	};

	file.close();
};