#define _USING_MATH_DEFINES

// ����������� ����������
#include <vector>
#include <cmath>
#include <string>

// ���������������� ����������
#include "printOut.hpp"

double Y0 = 0.5;
const int numSeries = 10000;

// �������� �������� ������-�������
double func(double y)
{
	double res = 0;
	for (int i = 0; i <= numSeries; i++)
		res += sin(i * Y0) * sin(i * y);
	res = res * 2.0 / (double)numSeries;
	return res;
};

int main()
{
	int M = 401;
	double Y = 1.0;
	double dy = Y / (M - 1);
	
	std::vector<double> dxVec(M);
	std::vector<double> deltaVec(M);
	
	for (int i = 0; i < M; i++)
	{
		dxVec[i] = i * dy;
		deltaVec[i] = func(i * dy);
	};
	
	std::vector< std::vector<double> > data = { dxVec, deltaVec };
	printOutWithoutHeader(std::to_string(M) + ".txt", 10, data, false);
	
	return 0;
};