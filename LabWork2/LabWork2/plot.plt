reset

set ylabel "x"
set xlabel "y"
set zlabel "u(x, y)"

set yrange [0:10]
#set xrange [0:50]

set palette rgbformulae 30, 31, 32
set pm3d interpolate 2, 2

splot 'NS_10000.txt' matrix with pm3d notitle