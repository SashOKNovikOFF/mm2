#define _USING_MATH_DEFINES

// ����������� ����������
#include <cmath>
#include <iostream>
#include <vector>

#include <Eigen\Dense>
#include "printOut.hpp"

using namespace std;
using namespace Eigen;

char* output = "NS_10000.txt"; // �������� ��������� �����

const int N = 50; // ����� ����� ����� �� ��� OX
const int M = 50; // ����� ����� ����� �� ��� OY

double X = 5.0; // ����� ����� �� ��� OX
double Y = 1.0; // ����� ����� �� ��� OY

double k = 10.0;       // ����������� ��� ������� (1 + epsilon * phi(x))
double epsilon = 0.1;  // ����������� ��� ������� phi(x)
double Y0 = 0.5;       // "����������" ������-������� �� ��� OY
int numSeries = 10000; // ����� ��������� ���� � ������������� ������-�������

// ������ ���� LU-�����������
void eigenSolver(double** matrix, double* freeVector, double delta_x, double delta_y)
{
	MatrixXd eigenMatrix(M * N, M * N);
	for (int i = 0; i < M * N; i++)
		for (int j = 0; j < M * N; j++)
			eigenMatrix(i, j) = matrix[i][j];

	VectorXd freeV(M * N);
	for (int i = 0; i < M * N; i++)
		freeV(i) = freeVector[i];

	VectorXd u = eigenMatrix.partialPivLu().solve(freeV);
	std::vector<double> dxVec(M * N);
	std::vector<double> dyVec(M * N);
	std::vector<double> solVec(M * N);

	MatrixXd solM(M, N);
	int I = 0;
	for (int i = 0; i < M; i++)
		for (int j = 0; j < N; j++)
		{
			solM(i, j) = u(I);
			//dxVec[I] = i * delta_y;
			//dyVec[I] = j * delta_x;
			//solVec[I] = u(I);
			I++;
		};
	
	printOutMatrix(output, solM);
	//std::vector< std::vector<double> > data = { dxVec, dyVec, solVec };
	//printOutWithoutHeader(output, 10, data, false);
};

// �������� �������� ������-�������
double func(double y)
{
	double res = 0;
	for (int i = 0; i <= numSeries; i++)
		res += sin(i * Y0) * sin(i * y);
	res = res * 2.0 / (double)numSeries;
	return res;
};

// �������� �������� ������� phi(x)
double phi(double y)
{
	double res;
	res = y * (1.0 - y) * pow((0.75 - y), 2.0);
	return res;
};

int main()
{
	double delta_x; // ������ ������ �� ��� OX
	double delta_y; // ������ ������ �� ��� OY

	// ��������� ������������ ������� ����
	double coef1;
	double coef2;
	
	double** matrix; // ������� ���� (M * N) x (M * N)
	double* U; // ������� �������� ���������� u(x, y)
	double* F; // �������� ������� ��������� ������

	// ������������� ������ �����
	delta_x = X / N;
	delta_y = Y / M;

	// ������������� ��������� ������������
	coef1 = 1.0 / delta_x / delta_x;
	coef2 = 1.0 / delta_y / delta_y;

	// ��������� ������ �� ������� u(x, y)
	matrix = (double **)malloc(sizeof(double *) * M * N);
	for (int i = 0; i < M * N; i++)
		matrix[i] = (double *)malloc(sizeof(double) * M * N);

	// ��������� ������ �� ������� ������� � ������� ��������� ������
	U = (double *)malloc(sizeof(double) * M * N);
	F = (double *)malloc(sizeof(double) * M * N);

	// �������� ������� u(x, y)
	for (int i = 0; i < M * N; i++)
		for (int j = 0; j < M * N; j++)
			matrix[i][j] = 0.0;

	// ��������� ��������� ���������
	for (int i = 0; i < M * N; i++)
		matrix[i][i] = 1.0;

	for (int i = M; i < M * (N - 1); i++) // ��������� �������, �� ���������� ������ � ������� ���� ���������� �����
	{
		if ((i % N == 0) || ((i + 1) % N == 0)) // ��������� ������� �� ����� � ������ ��������
		{
			matrix[i][i    ] = 1.0;
			matrix[i][i - 1] = matrix[i][i + 1] = 0.0;
			matrix[i][i - M] = matrix[i][i + M] = 0.0;
		}
		else // ��������� ������� � ��������� �����
		{
			matrix[i][i    ] = -2.0 / delta_x / delta_x + -2.0 / delta_y / delta_y + k * k * (1.0 + epsilon * phi(i * delta_y));
			matrix[i][i - 1] = matrix[i][i + 1] = coef2;
			matrix[i][i - M] = matrix[i][i + M] = coef1;
		};
	};

	// ��������� ������� ���� ���������� �����
	for (int i = M * (N - 1); i < M * N; i++)
		matrix[i][i - 1] = -1.0;

	// �������� ������� ��������� ������
	for (int i = 0; i < M * N; i++)
		F[i] = 0.0;

	// ��������� ������� ��������� ������
	for (int i = 0; i < M; i++)
		F[i] = func(i * delta_y);

	// ������� ���� LU-�����������
	eigenSolver(matrix, F, delta_x, delta_y);

	// ������������ ������
	for (int i = 0; i < M * N; i++)
		free(matrix[i]);
	free(matrix);
	free(U);
	free(F);

	return 0;
};